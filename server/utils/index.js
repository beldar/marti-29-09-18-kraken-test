const {
  existsSync, readdir, stat, unlink
} = require('fs');
const logger = require('morgan');

const getFileInfo = file => new Promise((resolve, reject) => {
  stat(file, (err, stats) => {
    if (err) {
      return reject(err);
    }

    return resolve(stats);
  });
});

const formatBytes = (bytes, decimals) => {
  if (bytes === 0) return '0 Bytes';
  const k = 1024;
  const dm = decimals <= 0 ? 0 : decimals || 2;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  const i = Math.floor(Math.log(bytes) / Math.log(k));
  return `${parseFloat(bytes / (k ** i)).toFixed(dm)} ${sizes[i]}`;
};

const getFiles = folder => new Promise((resolve, reject) => {
  if (existsSync(folder)) {
    readdir(folder, { withFileTypes: true }, (err, files) => {
      if (err) {
        logger('Error while reading upload folder', err);
        reject(err);
      }

      Promise.all(
        files.map(async (f) => {
          const stats = await getFileInfo(`${folder}/${f}`);

          return {
            name: f,
            sizeBites: stats.size,
            size: formatBytes(stats.size),
            lastModified: stats.mtime,
            created: stats.ctime
          };
        })
      )
        .then(resolve)
        .catch(reject);
    });
  } else {
    const error = `Upload directory ${folder} doesn't exist!`;
    logger(error);
    reject(error);
  }
});

const removeFile = path => new Promise((resolve, reject) => {
  unlink(path, (err) => {
    if (err) {
      logger(err);
      return reject(err);
    }
    resolve();
  });
});

module.exports = {
  getFiles,
  removeFile
};
