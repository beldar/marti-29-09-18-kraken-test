const express = require('express');
const { getFiles } = require('../utils');

const router = express.Router();

/* GET home page. */
router.get('/', async (req, res) => {
  const folder = `${__dirname}/../uploads`;
  const files = await getFiles(folder);
  res.render('index', { csrfToken: req.csrfToken(), files });
});

module.exports = { router };
