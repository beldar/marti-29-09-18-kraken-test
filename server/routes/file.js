const express = require('express');
const { resolve } = require('path');
const { existsSync } = require('fs');
const { getFiles, removeFile } = require('../utils');

const UPLOAD_FOLDER = `${__dirname}/../uploads`;
const router = express.Router();

/* Upload file */
router.post('/file', (req, res) => {
  if (!req.files || !req.files.file) {
    return res.status(500).send({ error: 'No file send.' });
  }

  const { file } = req.files;

  file.mv(resolve(`${UPLOAD_FOLDER}/${file.name}`), (err) => {
    if (err) {
      return res.status(500).send({ error: 'Upload failed.' });
    }

    getFiles(UPLOAD_FOLDER)
      .then(files => res.json({ files }))
      .catch((error) => {
        console.error(error);
        res.status(500).send({ error: 'Listing files failed' });
      });
  });
});

/* DELETE file */
router.delete('/file', async (req, res) => {
  const file = req.body.fileName;
  const fullPath = resolve(`${UPLOAD_FOLDER}/${file}`);

  if (!file || !existsSync(fullPath)) {
    return res.status(404).send({ error: 'File not found.' });
  }

  try {
    await removeFile(fullPath);
    const files = await getFiles(UPLOAD_FOLDER);
    res.json({ files });
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: 'Deleting file failed.' });
  }
});

/* GET files */
router.get('/files', (req, res) => {
  getFiles(resolve(UPLOAD_FOLDER))
    .then(files => res.json({ files }))
    .catch((err) => {
      console.error(err);
      res.status(500).send({ error: 'Listing files failed' });
    });
});

module.exports = { router };
