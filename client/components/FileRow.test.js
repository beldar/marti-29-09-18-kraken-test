import React from 'react';
import { shallow } from 'enzyme';
import FileRow from './FileRow';

describe('FileRow', () => {
  const file = {
    name: 'fileName',
    size: '1MB',
    lastModified: '2018-09-28T23:05:42.277Z'
  };

  it('should render correctly file information', () => {
    const expectedDate = new Date(file.lastModified).toLocaleString();
    const comp = shallow(<FileRow file={file} i={0} isBusy={false} deleteFile={(_ => _)} />);
    expect(comp.find('td').at(0).text()).toEqual('1');
    expect(comp.find('td').at(1).text()).toEqual(file.name);
    expect(comp.find('td').at(2).text()).toEqual(file.size);
    expect(comp.find('td').at(3).text()).toEqual(expectedDate);
    expect(comp.find('td').at(4).text()).toEqual('Delete');
    expect(comp).toMatchSnapshot();
  });

  it('should call deleteFile with the file name when clicking Delete if isBusy is false', () => {
    const deleteFile = jest.fn();
    /* eslint-disable func-names */
    const append = jest.fn();
    const FormDataMock = function () {
      this.append = append;
    };
    global.FormData = FormDataMock;
    const comp = shallow(<FileRow file={file} i={0} isBusy={false} deleteFile={deleteFile} />);
    comp.find('button').simulate('click');
    expect(append).toHaveBeenCalledWith('fileName', file.name);
    expect(deleteFile).toBeCalledWith(new FormDataMock());
  });

  it('should not call deleteFile if isBusy is true when clicking Delete', () => {
    const deleteFile = jest.fn();
    const comp = shallow(<FileRow file={file} i={0} isBusy deleteFile={deleteFile} />);
    comp.find('button').simulate('click');
    expect(deleteFile).not.toHaveBeenCalled();
  });
});
