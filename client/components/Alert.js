import React from 'react';
import PropTypes from 'prop-types';

const Alert = ({ apiSuccess, apiError }) => {
  let classes = 'alert';

  if (!apiSuccess && !apiError) return null;
  if (apiSuccess) classes += ' alert-success';
  if (apiError) classes += ' alert-danger';

  return (
    <div className={classes} role="alert">
      {apiSuccess || apiError}
    </div>
  );
};

Alert.propTypes = {
  apiSuccess: PropTypes.string,
  apiError: PropTypes.string
};

Alert.defaultProps = {
  apiSuccess: '',
  apiError: ''
};

export default Alert;
