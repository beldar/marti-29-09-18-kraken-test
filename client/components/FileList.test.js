import React from 'react';
import { shallow } from 'enzyme';
import FileList from './FileList';

describe('FileList', () => {
  it('should render correctly a list of files', () => {
    const file1 = { name: 'file1' };
    const file2 = { name: 'file2' };
    const isBusy = false;
    const deleteFile = _ => _;
    const files = [file1, file2];
    const comp = shallow(<FileList files={files} isBusy={isBusy} deleteFile={deleteFile} />);
    expect(comp.find('FileRow').length).toEqual(2);
    expect(comp.find('FileRow').at(0).props()).toEqual({
      file: file1,
      isBusy,
      deleteFile,
      i: 0
    });
    expect(comp.find('FileRow').at(1).props()).toEqual({
      file: file2,
      isBusy,
      deleteFile,
      i: 1
    });
    expect(comp).toMatchSnapshot();
  });
});
