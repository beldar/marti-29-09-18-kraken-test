import React from 'react';
import FileList from './FileList';
import FileUploader from './FileUploader';
import Alert from './Alert';

const FileManager = props => (
  <div id="fileManager">
    <Alert {...props} />
    <FileUploader {...props} />
    <FileList {...props} />
  </div>
);

export default FileManager;
