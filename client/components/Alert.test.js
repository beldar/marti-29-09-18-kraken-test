import React from 'react';
import { shallow } from 'enzyme';
import Alert from './Alert';

describe('Alert', () => {
  it('should return null if no success nor error message exist', () => {
    const comp = shallow(<Alert />);
    expect(comp.type()).toEqual(null);
  });

  it('should render correctly a success message', () => {
    const message = 'Everything is great';
    const comp = shallow(<Alert apiSuccess={message} />);
    expect(comp.find('.alert-success').length).toEqual(1);
    expect(comp.find('.alert-success').text()).toEqual(message);
    expect(comp).toMatchSnapshot();
  });

  it('should render correctly an error message', () => {
    const message = 'An error ocurred';
    const comp = shallow(<Alert apiError={message} />);
    expect(comp.find('.alert-danger').length).toEqual(1);
    expect(comp.find('.alert-danger').text()).toEqual(message);
    expect(comp).toMatchSnapshot();
  });
});
