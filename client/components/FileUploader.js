import React, { Component } from 'react';
import PropTypes from 'prop-types';

class FileUploader extends Component {
  handleUpload = (e) => {
    e.preventDefault();
    const {
      isBusy,
      uploadFile
    } = this.props;

    if (isBusy || !this.uploadInput.files[0]) return false;

    const data = new FormData();
    data.append('file', this.uploadInput.files[0]);

    uploadFile(data);
  }

  render() {
    const { csrfToken, isBusy } = this.props;

    return (
      <form method="POST" action="/api/file" onSubmit={this.handleUpload}>
        <h2>Upload a new file</h2>
        <input type="hidden" name="_csrf" value={csrfToken} />
        <div className="form-row align-items-center">
          <div className="col-auto">
            <input
              type="file"
              className="form-control mb-2"
              id="newFile"
              disabled={isBusy}
              ref={(c) => { this.uploadInput = c; }}
            />
          </div>
          <div className="col-auto">
            <button
              disabled={isBusy}
              type="submit"
              className="btn btn-primary mb-2"
            >
              { isBusy ? 'Uploading...' : 'Upload' }
            </button>
          </div>
        </div>
      </form>
    );
  }
}

FileUploader.propTypes = {
  csrfToken: PropTypes.string.isRequired,
  isBusy: PropTypes.bool.isRequired,
  uploadFile: PropTypes.func.isRequired
};

export default FileUploader;
