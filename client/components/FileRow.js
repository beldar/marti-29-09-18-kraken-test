import React from 'react';
import PropTypes from 'prop-types';

const FileRow = ({
  file,
  i,
  isBusy,
  deleteFile
}) => (
  <tr key={file.name}>
    <td>{i + 1}</td>
    <td>{file.name}</td>
    <td>{file.size}</td>
    <td>{new Date(file.lastModified).toLocaleString()}</td>
    <td>
      <button
        disabled={isBusy}
        type="button"
        className="btn btn-danger"
        onClick={() => {
          if (isBusy) return false;
          const data = new FormData();
          data.append('fileName', file.name);
          deleteFile(data);
        }}
      >
        Delete
      </button>
    </td>
  </tr>
);

FileRow.propTypes = {
  file: PropTypes.object.isRequired,
  i: PropTypes.number.isRequired,
  isBusy: PropTypes.bool.isRequired,
  deleteFile: PropTypes.func.isRequired
};

export default FileRow;
