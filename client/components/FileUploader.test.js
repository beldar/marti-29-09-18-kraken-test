import React from 'react';
import { shallow } from 'enzyme';
import FileUploader from './FileUploader';

describe('FileUploader', () => {
  it('should render correctly', () => {
    const comp = shallow(<FileUploader uploadFile={_ => _} csrfToken="" isBusy={false} />);
    expect(comp).toMatchSnapshot();
  });

  it('should call uploadFile if isBusy is false and a file exists', () => {
    /* eslint-disable func-names */
    const uploadFile = jest.fn();
    const append = jest.fn();
    const FormDataMock = function () {
      this.append = append;
    };
    global.FormData = FormDataMock;
    FileUploader.prototype.uploadInput = { files: [{}] };
    const comp = shallow(<FileUploader uploadFile={uploadFile} csrfToken="" isBusy={false} />);
    const preventDefault = jest.fn();
    comp.find('form').simulate('submit', { preventDefault });
    expect(preventDefault).toHaveBeenCalled();
    expect(append).toHaveBeenCalledWith('file', {});
    expect(uploadFile).toHaveBeenCalledWith(new FormDataMock());
  });

  it('should not call uploadFile if isBusy is true', () => {
    /* eslint-disable func-names */
    const uploadFile = jest.fn();
    const append = jest.fn();
    const FormDataMock = function () {
      this.append = append;
    };
    global.FormData = FormDataMock;
    FileUploader.prototype.uploadInput = { files: [{}] };
    const comp = shallow(<FileUploader uploadFile={uploadFile} csrfToken="" isBusy />);
    const preventDefault = jest.fn();
    comp.find('form').simulate('submit', { preventDefault });
    expect(preventDefault).toHaveBeenCalled();
    expect(append).not.toHaveBeenCalled();
    expect(uploadFile).not.toHaveBeenCalled();
  });

  it('should not call uploadFile if there are no files', () => {
    /* eslint-disable func-names */
    const uploadFile = jest.fn();
    const append = jest.fn();
    const FormDataMock = function () {
      this.append = append;
    };
    global.FormData = FormDataMock;
    FileUploader.prototype.uploadInput = { files: [] };
    const comp = shallow(<FileUploader uploadFile={uploadFile} csrfToken="" isBusy={false} />);
    const preventDefault = jest.fn();
    comp.find('form').simulate('submit', { preventDefault });
    expect(preventDefault).toHaveBeenCalled();
    expect(append).not.toHaveBeenCalled();
    expect(uploadFile).not.toHaveBeenCalled();
  });
});
