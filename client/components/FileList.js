import React from 'react';
import PropTypes from 'prop-types';
import FileRow from './FileRow';

const FileList = ({ files, isBusy, deleteFile }) => (
  <div id="fileList">
    <h2>File List</h2>
    <table className="table table-bordered">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Size</th>
          <th scope="col">Last uploaded</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>
      <tbody>
        {
          files.map((file, i) => (
            <FileRow
              key={file.name}
              file={file}
              i={i}
              isBusy={isBusy}
              deleteFile={deleteFile}
            />
          ))
        }
      </tbody>
    </table>
  </div>
);

FileList.propTypes = {
  files: PropTypes.array.isRequired,
  isBusy: PropTypes.bool.isRequired,
  deleteFile: PropTypes.func.isRequired
};

export default FileList;
