import React from 'react';
import { shallow } from 'enzyme';
import FileManager from './FileManager';

describe('FileManager', () => {
  it('should render Alert, FileUploader and FileList', () => {
    const props = {
      csrfToken: '',
      files: [],
      isBusy: false,
      apiSuccess: '',
      apiError: '',
      uploadFile: _ => _,
      deleteFile: _ => _
    };
    const comp = shallow(<FileManager {...props} />);
    expect(comp.find('Alert').length).toEqual(1);
    expect(comp.find('Alert').props()).toEqual(props);
    expect(comp.find('FileUploader').length).toEqual(1);
    expect(comp.find('FileUploader').props()).toEqual(props);
    expect(comp.find('FileList').length).toEqual(1);
    expect(comp.find('FileList').props()).toEqual(props);
    expect(comp).toMatchSnapshot();
  });
});
