import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import {
  uploadFile,
  deleteFile,
  BEGIN_UPLOAD,
  UPLOAD_SUCCESS,
  SET_FILES,
  UPLOAD_FAILURE,
  CLEAR_MESSAGES,
  DELETE_FAILURE,
  DELETE_SUCCESS,
  BEGIN_DELETE
} from './index';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('async actions', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  describe('uploadFile', () => {
    it('should dispatch the right actions when successfuly uploading a file', () => {
      fetchMock
        .postOnce('/api/file', { body: { files: [{}] }, headers: { 'content-type': 'application/json' } });

      const expectedActions = [
        { type: BEGIN_UPLOAD },
        { type: UPLOAD_SUCCESS, payload: 'File uploaded successfuly!' },
        { type: SET_FILES, payload: [{}] },
        { type: CLEAR_MESSAGES }
      ];

      const store = mockStore({ files: [] });

      return store.dispatch(uploadFile({})).then(() => {
        jest.runAllTimers();
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('should dispatch the right actions when uploading a file fails', () => {
      const errorMessage = 'Something went wrong';
      fetchMock
        .postOnce('/api/file', { status: 500, body: { error: errorMessage }, headers: { 'content-type': 'application/json' } });

      const expectedActions = [
        { type: BEGIN_UPLOAD },
        { type: UPLOAD_FAILURE, payload: errorMessage },
        { type: CLEAR_MESSAGES }
      ];

      const store = mockStore({ files: [] });

      return store.dispatch(uploadFile({})).then(() => {
        jest.runAllTimers();
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('deleteFile', () => {
    it('should dispatch the right actions when successfuly uploading a file', () => {
      fetchMock
        .deleteOnce('/api/file', { body: { files: [{}] }, headers: { 'content-type': 'application/json' } });

      const expectedActions = [
        { type: BEGIN_DELETE },
        { type: DELETE_SUCCESS, payload: 'File deleted successfuly!' },
        { type: SET_FILES, payload: [{}] },
        { type: CLEAR_MESSAGES }
      ];

      const store = mockStore({ files: [] });

      return store.dispatch(deleteFile({})).then(() => {
        jest.runAllTimers();
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('should dispatch the right actions when uploading a file fails', () => {
      const errorMessage = 'Something went wrong';
      fetchMock
        .deleteOnce('/api/file', { status: 500, body: { error: errorMessage }, headers: { 'content-type': 'application/json' } });

      const expectedActions = [
        { type: BEGIN_DELETE },
        { type: DELETE_FAILURE, payload: errorMessage },
        { type: CLEAR_MESSAGES }
      ];

      const store = mockStore({ files: [] });

      return store.dispatch(deleteFile({})).then(() => {
        jest.runAllTimers();
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});
