export const SET_FILES = 'SET_FILES';
export const BEGIN_UPLOAD = 'BEGIN_UPLOAD';
export const UPLOAD_SUCCESS = 'UPLOAD_SUCCESS';
export const UPLOAD_FAILURE = 'UPLOAD_FAILURE';
export const BEGIN_DELETE = 'BEGIN_DELETE';
export const DELETE_SUCCESS = 'DELETE_SUCCESS';
export const DELETE_FAILURE = 'DELETE_FAILURE';
export const CLEAR_MESSAGES = 'CLEAR_MESSAGES';
export const MESSAGE_TIME = 3000; // 3seconds

export const setFiles = files => ({
  type: SET_FILES,
  payload: files
});

export const beginUpload = () => ({
  type: BEGIN_UPLOAD
});

export const uploadSuccess = message => ({
  type: UPLOAD_SUCCESS,
  payload: message
});

export const uploadFailure = error => ({
  type: UPLOAD_FAILURE,
  payload: error
});

export const beginDelete = () => ({
  type: BEGIN_DELETE
});

export const deleteSuccess = message => ({
  type: DELETE_SUCCESS,
  payload: message
});

export const deleteFailure = error => ({
  type: DELETE_FAILURE,
  payload: error
});

export const clearMessages = () => ({
  type: CLEAR_MESSAGES
});

let timer;

export const uploadFile = data => (dispatch, getState) => {
  clearTimeout(timer);
  dispatch(beginUpload());
  return fetch('/api/file', {
    method: 'POST',
    body: data,
    credentials: 'same-origin',
    headers: {
      'CSRF-Token': getState().files.csrfToken
    }
  })
    .then((r) => {
      if (r.status >= 400) {
        return r.json().then((error) => { throw new Error(error.error); });
      }
      return r;
    })
    .then(r => r.json())
    .then(({ files }) => {
      dispatch(uploadSuccess('File uploaded successfuly!'));
      dispatch(setFiles(files));
      timer = setTimeout(() => dispatch(clearMessages()), MESSAGE_TIME);
    })
    .catch((error) => {
      console.error(error);
      dispatch(uploadFailure(error.message));
      timer = setTimeout(() => dispatch(clearMessages()), MESSAGE_TIME);
    });
  // This is not supported by the test framework :(
  // Submitted an issue for it https://github.com/wheresrhys/fetch-mock/issues/373
  // .finally(() => {
  //   timer = setTimeout(() => dispatch(clearMessages()), MESSAGE_TIME);
  // });
};

export const deleteFile = data => (dispatch, getState) => {
  clearTimeout(timer);
  dispatch(beginDelete());
  return fetch('/api/file', {
    method: 'DELETE',
    body: data,
    credentials: 'same-origin',
    headers: {
      'CSRF-Token': getState().files.csrfToken
    }
  })
    .then((r) => {
      if (r.status >= 400) {
        return r.json().then((error) => { throw new Error(error.error); });
      }
      return r;
    })
    .then(r => r.json())
    .then(({ files }) => {
      dispatch(deleteSuccess('File deleted successfuly!'));
      dispatch(setFiles(files));
      timer = setTimeout(() => dispatch(clearMessages()), MESSAGE_TIME);
    })
    .catch((error) => {
      console.error(error);
      dispatch(deleteFailure(error.message));
      timer = setTimeout(() => dispatch(clearMessages()), MESSAGE_TIME);
    });
  // .finally(() => {
  //   timer = setTimeout(() => dispatch(clearMessages()), MESSAGE_TIME);
  // });
};
