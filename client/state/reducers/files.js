import {
  SET_FILES,
  BEGIN_UPLOAD,
  UPLOAD_SUCCESS,
  UPLOAD_FAILURE,
  BEGIN_DELETE,
  DELETE_SUCCESS,
  DELETE_FAILURE,
  CLEAR_MESSAGES
} from '../actions';

const defaultState = {
  csrfToken: null,
  files: [],
  isBusy: false,
  apiSuccess: false,
  apiError: false
};

const files = (state = defaultState, action) => {
  switch (action.type) {
    case SET_FILES:
      return Object.assign({}, state, {
        files: action.payload
      });
    case BEGIN_UPLOAD:
      return Object.assign({}, state, {
        isBusy: true,
        apiSuccess: false,
        apiError: false
      });
    case UPLOAD_SUCCESS:
      return Object.assign({}, state, {
        isBusy: false,
        apiSuccess: action.payload
      });
    case UPLOAD_FAILURE:
      return Object.assign({}, state, {
        isBusy: false,
        apiError: action.payload
      });
    case BEGIN_DELETE:
      return Object.assign({}, state, {
        isBusy: true,
        apiSuccess: false,
        apiError: false
      });
    case DELETE_SUCCESS:
      return Object.assign({}, state, {
        isBusy: false,
        apiSuccess: action.payload
      });
    case DELETE_FAILURE:
      return Object.assign({}, state, {
        isBusy: false,
        apiError: action.payload
      });
    case CLEAR_MESSAGES:
      return Object.assign({}, state, {
        apiError: false,
        apiSuccess: false
      });
    default:
      return state;
  }
};

export default files;
