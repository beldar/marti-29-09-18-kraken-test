import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import FileManager from '../components/FileManager';
import * as FileActions from '../state/actions';

const mapStateToProps = state => ({
  files: state.files.files,
  csrfToken: state.files.csrfToken,
  isBusy: state.files.isBusy,
  apiError: state.files.apiError,
  apiSuccess: state.files.apiSuccess
});

const mapDispatchToProps = dispatch => bindActionCreators(FileActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FileManager);
