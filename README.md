# Kraken Test

Write an application which allows users to upload (and manage) multiple documents to an application.

Your application should handle the following use cases:

1. Upload an individual document
2. List previously uploaded documents
3. Delete a document

## Approach

When starting a new application from scratch I normally approach it by using the bare minimum number of things that I need to accomplish the task that I'm given.

Because applications tend to grow very quickly in features and files, by not having a lot of bloat at the begining it allows for a controlled growth and quick refactor.

With the same idea I've engineered the tooling around it. Instead of starting with a boilerplate that could do a lot of fancy things with lots of plugins, I've opted for an extrmely simple webpack configuration (arguably the simplest you can get), that would simply compile my frontend code to a bundle file, no transpiling needed on the backend.

In this project I've used Bootstrap for quick layout and structure, I want to clarify that I would never use Bootstrap in production, but I think its a useful tool to prototype something quickly when you don't have any designs to work with. In my experience, if I try to create a brand new design from scracth, it takes me several times longer and it never looks quite as good.


## Features

### App

* Upload files via Ajax
* List files with stats (uploaded date, size)
* Race condition protection by disabling user action until async tasks are completed
* Alert message for success or failure outcome from user action
* Redux thunk action creators for API interaction
* Preloaded state from the server for initial file list and CSRF Token
* Code linted with best practices

### Security

* CORS headers with only `http://localhost` allowed
* CSRF token protection and cookie when uploading and deleting files
* XSS protection headers
* Plain text errors, only error details in development
* Remove "powered by" headers

### Testing

Since the main focus of this test is the frontend, and in the interest of time, I've opted only to test the most important bits of code on the frontend. Showcasing things like snapshots, shallow render tests, and testing of asynchronous thunks.

## Install

```
yarn install
```

## Run

```
yarn start
```

## Develop

```
yarn dev
```

## Test

```
yarn test
```

## Lint

```
yarn lint
```
